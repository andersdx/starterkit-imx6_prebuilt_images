#!/bin/bash

NORMAL="\033[0m" 
GREEN="\033[32;1m"


title() {
	echo -e "${GREEN}===${1}===${NORMAL}"
}

announce() {
	echo -e "${GREEN}* ${NORMAL}${1}"
}


echo " "
title " Anders Installation Script Executing. Please wait... " 


echo " "
#announce "Mounting SD Card " 
mount /dev/mmcblk2p1 /media/mmcblk0p1


echo " "
announce "Installing Kernel Files"
flash_erase /dev/mtd3 0 0
nandwrite -p /dev/mtd3 /media/mmcblk0p1/uImage-cm-fx6


echo " "
announce "Installing OS"
flash_erase /dev/mtd4 0 0
ubiformat /dev/mtd4
ubiattach -m 4 -d 0
ubimkvol /dev/ubi0 -m -N rootfs
mkdir -p /media/rootfs && mount -t ubifs ubi0:rootfs /media/rootfs

echo " "
echo "Extracting user space. Please Wait..."
tar -xpzf /media/mmcblk0p1/anders-image.tar.gz -C /media/rootfs && sync
echo "Done Extracting"

echo " "
#announce "Unmount SD Card "
umount /media/rootfs

echo " "
echo " "
title "======================================="
echo " "
echo " "
announce "Anders Installation Script COMPLETED!"
echo " "
announce "Please remove installation sd card "
announce "and reboot the board."
echo " "
echo " "
title "======================================="
echo " "
echo " "
echo " "



exit 0
