Anders scripts generation instruction:

1. Edit the source file present in "source_script" folder
2. Enter the "source_script" folder
3. run command:
			mkimage -A arm -C none -T script -d dx6_boot_10.1.script ../boot.scr
4. Place the output boot.scr file in the Sd card along with the rest of the images.