==============================
Anders DX6 Pre-Built SW Images 
==============================
     = RELEASE NOTES =




> DX6 Release Package v.1.0 <

- Features Added
  1. 7.0", 10.1", 12.1" Anders Display support
  2. Anders Touchscreen Driver support
  3. QT libraries v.4.8
  4. Basic QT Demo Application

- Fixes
     N/A

- Known Issues
  1. QT v.4.8 touchscreen responsivness
  2. Demo app general instability
